from django.urls import path

from main import views

urlpatterns = [
    path('',  views.Listjuga.as_view()),
    path('listjuga/', views.Listjuga.as_view(), name="list_jugadores"),
    path('liste/', views.ListEqui.as_view(), name="liste"),
    path('listesta/', views.ListEsta.as_view(), name="listesta"),

    path('detailj/<int:pk>', views.DetailJ.as_view(), name="detailj"),
    path('detailE/<int:pk>', views.DetailE.as_view(), name="detailE"),
    path('detailES/<int:pk>', views.DetailES.as_view(), name="detailES"),

    path('searchJE', views.JugadorEqui.as_view(), name="searchJE"),
]
