from django.contrib import admin

from .models import Equipos, Jugadores, Estadios
# Register your models here.


@admin.register(Equipos)
class AdminEquipos(admin.ModelAdmin):
	list_display = [
		"Nombre",
		"Estadio",
		"Copas",
	]

@admin.register(Jugadores)
class AdminJuga(admin.ModelAdmin):
    list_display = [
        "Nombre",
        "Estatura",
        "Peso",
    ]

@admin.register(Estadios)
class AdminEstadio(admin.ModelAdmin):
    list_display = [
        "Nombre",
        "Ubicacion",
    ]