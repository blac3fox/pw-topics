from django.db import models

from django.conf import settings
# Create your models here.
class Estadios(models.Model):
    Nombre = models.CharField(max_length=50)
    Ubicacion = models.CharField(max_length=50)
    CantidadPersonas = models.IntegerField(default=1)
    slug = models.SlugField()

    def __str__(self):
        return self.Nombre

class Equipos(models.Model):
    Nombre = models.CharField(max_length=50)
    Estadio = models.ForeignKey(Estadios, on_delete=models.CASCADE ,default=1)
    Copas = models.IntegerField()
    slug = models.SlugField()

    def __str__(self):
        return self.Nombre

class Jugadores(models.Model):
    Nombre = models.CharField(max_length=50)
    Estatura = models.FloatField()
    Peso = models.IntegerField()
    Edad = models.IntegerField()
    Posicion = models.CharField(max_length=50, choices=[('QB', 'quarterback'), ("RB","Running back")])
    Equipo = models.ForeignKey(Equipos, on_delete=models.CASCADE, default=1)
    Status = models.BooleanField(default=0)
    slug = models.SlugField()

    def __str__(self):
        return self.Nombre



