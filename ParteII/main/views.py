from django.shortcuts import render, redirect
from django.views import generic
from django.urls import reverse_lazy

# Create your views here.
from .models import Jugadores, Estadios, Equipos


# CRUD

# RETRIEVE
#Lista Jugadores

class Listjuga(generic.ListView):
	template_name = "main/list.html"

	def get_queryset(self, *args, **kwargs):
		qs = Jugadores.objects.all()
		print(self.request)
		return qs

	def get_context_data(self, *args, **kwargs):
		context = super(Listjuga, self).get_context_data(*args, **kwargs)  # context = get_context_data()
		return context

#Detai Jugadores

class DetailJ(generic.DetailView):
	template_name = "main/detailj.html"
	model = Jugadores

#Lista Equipo

class ListEqui(generic.ListView):
	template_name = "main/listEqui.html"

	def get_queryset(self, *args, **kwargs):
		qs = Equipos.objects.all()
		print(self.request)
		return qs

	def get_context_data(self, *args, **kwargs):
		context = super(ListEqui, self).get_context_data(*args, **kwargs)  # context = get_context_data()
		return context
#Detail Equipo

class DetailE(generic.DetailView):
	template_name = "main/detailE.html"
	model = Equipos

#Lista Estadio

class ListEsta(generic.ListView):
	template_name = "main/listEsta.html"

	def get_queryset(self, *args, **kwargs):
		qs = Estadios.objects.all()
		print(self.request)
		return qs

	def get_context_data(self, *args, **kwargs):
		context = super(ListEsta, self).get_context_data(*args, **kwargs)  # context = get_context_data()
		return context

#Detail Estadio
class DetailES(generic.DetailView):
	template_name = "main/detailES.html"
	model = Estadios


##Buscar Jugador Por equipo
class JugadorEqui(generic.ListView):
    template_name = 'main/search1.html'
    model = Jugadores

    def get_queryset(self):
        query = self.request.GET.get('q')
        if query:
            object_list = self.model.objects.filter(Equipo_id__exact=query)
        else:
            object_list = self.model.objects.none()
        return object_list